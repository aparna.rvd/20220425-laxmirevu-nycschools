//
//  WebProvider.swift
//  20220425-LaxmiRevu-NYCSchools
//
//  Created by Laxmi Revu on 4/25/22.
//

import Foundation

import Combine
import Foundation

enum FetchError: Error {
    case invalidUrl
    case responseFailure
}

enum Constants {
    static let schoolAPIHost = "data.cityofnewyork.us"
    static let searchSchoolPath = "/resource/s3k6-pzi2.json"
    static let searchSATScorePath = "/resource/f9bf-2cp4.json"
}

class WebProvider: NSObject {
    
    // Fetch High school data
    func fetchHighSchoolDirectoryPublisher() -> AnyPublisher<[School], FetchError> {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = Constants.schoolAPIHost
        urlComponents.path = Constants.searchSchoolPath
        
        guard let url = urlComponents.url else {
            return Fail(error: .invalidUrl)
                .eraseToAnyPublisher()
        }
        let urlRequest = URLRequest(url: url)
        
        return URLSession.shared.dataTaskPublisher(for: urlRequest)
            .map(\.data)
            .decode(type: [School].self, decoder: JSONDecoder())
            .mapError { _ in .responseFailure }
            .eraseToAnyPublisher()
    }
    
    // Fetch high school SAT scores
    func fetchSATScorePublisher() -> AnyPublisher<[SATScore], FetchError> {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = Constants.schoolAPIHost
        urlComponents.path = Constants.searchSATScorePath
        
        guard let url = urlComponents.url else {
            return Fail(error: .invalidUrl)
                .eraseToAnyPublisher()
        }
        let urlRequest = URLRequest(url: url)
        
        return URLSession.shared.dataTaskPublisher(for: urlRequest)
            .map(\.data)
            .decode(type: [SATScore].self, decoder: JSONDecoder())
            .mapError { _ in .responseFailure }
            .eraseToAnyPublisher()
    }
}
