//
//  SchoolViewController.swift
//  20220425-LaxmiRevu-NYCSchools
//
//  Created by Laxmi Revu on 4/25/22.
//

import UIKit

private let tableCellHeight: CGFloat = 160.0

class SchoolViewController: UIViewController {

    private var viewModel: SchoolViewModel!

    @IBOutlet private var tableView: UITableView!
    // Adding refresh control to show the loading indicator
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handlePullToRefresh(_:)),
                                 for: .valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = SchoolViewModel()
        self.configureTableViewData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
}

// MARK: - TableView Data source and Delegate
extension SchoolViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.highSchools.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: SchoolTableViewCell.reuseIdentifier, for: indexPath) as? SchoolTableViewCell,
            indexPath.row < viewModel.highSchools.count
        else { return UITableViewCell() }

        let schoolInfo = viewModel.highSchools[indexPath.row]
        cell.configure(with: schoolInfo)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableCellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let schoolInfo = viewModel.highSchools[indexPath.row]
        let satScores = viewModel.satScores.filter { $0.id == schoolInfo.id }
        let detailVC = SchoolDetailsViewController(viewModel: SchoolDetailsViewModel(school: schoolInfo, satScore: satScores.isEmpty ? nil : satScores[0]))
        navigationController?.pushViewController(detailVC, animated: true)
    }
}

// MARK: - Utility Methods
extension SchoolViewController {
    
    // Configure the tableview
    func configureTableViewData() {
        self.tableView.tableFooterView = UIView.init(frame: .zero)
        self.tableView.register(UINib.init(nibName: SchoolTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: SchoolTableViewCell.reuseIdentifier)

        // Safe check for below iOS 10
        if #available(iOS 10.0, *) {
            tableView.refreshControl = self.refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        self.refreshControl.beginRefreshing()

        self.viewModel.highSchoolInfoDidChange = {
            DispatchQueue.main.async {
                if self.refreshControl.isRefreshing {
                    self.refreshControl.endRefreshing()
                }
                self.tableView.reloadData()
            }
        }
    }
    
    // Pull to refresh to fetch the latest data
    @objc private func handlePullToRefresh(_ sender: Any) {
        self.viewModel.fetchHighSchools()
    }
}
