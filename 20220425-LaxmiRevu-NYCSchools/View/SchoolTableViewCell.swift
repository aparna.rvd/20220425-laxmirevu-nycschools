//
//  SchoolTableViewCell.swift
//  20220425-LaxmiRevu-NYCSchools
//
//  Created by Laxmi Revu on 4/26/22.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    static var reuseIdentifier: String { "SchoolTableViewCell" }

    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var websiteLabel: UILabel!
    @IBOutlet private var addressLabel: UILabel!
    @IBOutlet private var contentLabel: UILabel!
    @IBOutlet private var totalStudentsLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(with schoolInfo: School) {
        nameLabel.text = schoolInfo.schoolName
        websiteLabel.text = schoolInfo.website
        addressLabel.text = schoolInfo.address
        contentLabel.text = schoolInfo.academicopportunities1
        totalStudentsLabel.text = "\(schoolInfo.grades2018) | \(schoolInfo.totalStudents)"
    }
}
