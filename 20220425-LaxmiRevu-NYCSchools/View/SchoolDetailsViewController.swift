//
//  SchoolDetailsViewController.swift
//  20220425-LaxmiRevu-NYCSchools
//
//  Created by Laxmi Revu on 4/25/22.
//

import UIKit

private let bottomSpaceHeight: CGFloat = 100
private let navigationTitle: String = "School Details"

class SchoolDetailsViewController: UIViewController {
    
    private let viewModel: SchoolDetailsViewModel
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var totalStudentsLabel: UILabel!
    @IBOutlet weak var mathScoreSlider: CustomSlider!
    @IBOutlet weak var writingScoreSlider: CustomSlider!
    @IBOutlet weak var readingScoreSlider: CustomSlider!
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!

    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var academicLabel: UILabel!
    @IBOutlet weak var transportLabel: UILabel!
    @IBOutlet weak var languagesLabel: UILabel!

    init(viewModel: SchoolDetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: String(describing: type(of: self)), bundle: Bundle.main)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = navigationTitle
        self.showSelectedSchoolDetails()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scrollView.contentSize = CGSize(width: self.view.bounds.size.width, height: self.view.bounds.size.height + bottomSpaceHeight)
    }
}

// MARK: - Utility Methods
extension SchoolDetailsViewController {
    
    func showSelectedSchoolDetails() {
        self.schoolNameLabel.text = self.viewModel.schoolName
        self.totalStudentsLabel.text = "\(self.viewModel.schoolDetails.grades2018) | \(self.viewModel.schoolDetails.totalStudents)"
        self.addressLabel.text = self.viewModel.contactUS
        self.descriptionLabel.text = self.viewModel.description
        
        if let satScores = self.viewModel.satScoreDetails {
            self.mathScoreSlider.value = Float(satScores.mathScore)
            self.readingScoreSlider.value =  Float(satScores.readingScore)
            self.writingScoreSlider.value = Float(satScores.writingScore)
            self.mathScoreLabel.text = "\(satScores.mathScore)"
            self.readingScoreLabel.text = "\(satScores.readingScore)"
            self.writingScoreLabel.text = "\(satScores.writingScore)"

            self.mathScoreSlider.updateSliderColor()
            self.readingScoreSlider.updateSliderColor()
            self.writingScoreSlider.updateSliderColor()
        }
        self.academicLabel.text = self.viewModel.schoolDetails.academicopportunities1
        self.transportLabel.text = self.viewModel.transport
        self.languagesLabel.text = self.viewModel.language
        self.customView.layer.cornerRadius = 8.0
        self.customView.layer.borderColor = UIColor.gray.cgColor
        self.customView.layer.borderWidth = 1
    }
}

extension SchoolDetailsViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //Scrolling needs to handle
    }
}
