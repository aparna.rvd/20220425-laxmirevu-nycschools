//
//  SchoolDetailsViewModel.swift
//  20220425-LaxmiRevu-NYCSchools
//
//  Created by Laxmi Revu on 4/26/22.
//

import UIKit

class SchoolDetailsViewModel {
    var schoolDetails: School
    var satScoreDetails: SATScore?

    var schoolName: String { schoolDetails.schoolName }
    var address: String { schoolDetails.address }
    var description: String { schoolDetails.description }
    var contactUS: String { schoolDetails.contactUS }
    var  transport: String { schoolDetails.transport }
    var  language: String {
        guard let language = schoolDetails.language else {
            return "Language: NA"
        }
        return "Language: \(language)"
    }

    init(school: School, satScore: SATScore?) {
        self.schoolDetails = school
        self.satScoreDetails = satScore
    }
}

