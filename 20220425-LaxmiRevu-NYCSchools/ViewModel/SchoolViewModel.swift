//
//  SchoolViewModel.swift
//  20220425-LaxmiRevu-NYCSchools
//
//  Created by Laxmi Revu on 4/25/22.
//

import Combine
import Foundation

class SchoolViewModel {
    private var webProvider: WebProvider!
    public var highSchools: [School] = [] {
        didSet { highSchoolInfoDidChange?() }
    }

    public var satScores: [SATScore] = [] {
        didSet { satScoreDetailssDidChange?() }
    }
    private var cancellables = Set<AnyCancellable>()
    var highSchoolInfoDidChange: (() -> ())?
    var satScoreDetailssDidChange: (() -> ())?

    init() {
        self.webProvider = WebProvider()
        self.fetchHighSchools()
        self.fetchHighSchoolSATScore()
    }
    
    func fetchHighSchools() {
//        webProvider.fetchHighSchoolsDirectory { response, error in
//            if let error = error {
//                print("An error occurred while fetching 2017 high school data.", error)
//            }
//            guard let schools = response else {
//                return
//            }
//            self.highSchools = schools
//            print("schoolsInfo : \(schools)")
//        }
        webProvider.fetchHighSchoolDirectoryPublisher()
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { completion in
                    if case .failure = completion {
                        print("An error occurred while fetching 2017 high school data.")
                    }
                }, receiveValue: { schoolsInfo in
                    print("schoolsInfo : \(schoolsInfo.count)")
                    self.highSchools = schoolsInfo
                }
            )
            .store(in: &cancellables)
    }
    
    func fetchHighSchoolSATScore() {
        webProvider.fetchSATScorePublisher()
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { completion in
                    if case .failure = completion {
                        print("An error occurred while fetching SAT score data.")
                    }
                }, receiveValue: { satScores in
                    print("satScores : \(satScores.count)")
                    self.satScores = satScores
                }
            )
            .store(in: &cancellables)

    }
}
