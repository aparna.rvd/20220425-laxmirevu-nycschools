//
//  School.swift
//  20220425-LaxmiRevu-NYCSchools
//
//  Created by Laxmi Revu on 4/25/22.
//

import Foundation

struct SchoolDirectory: Decodable {
    let schools: [School]
}

struct School: Decodable {
    
    let id : String
    let schoolName: String
    let boro: String
    let description: String
    let school10thSeats: String?
    let academicopportunities1: String?
    let ellPrograms: String?
    let neighborhood: String?
    let building_code: String?
    let location: String?
    let phoneNumber: String?
    let faxNumber: String?
    let schoolEmail: String?
    let website: String?
    let subway: String?
    let bus: String?
    let grades2018: String
    let finalgrades: String?
    let totalStudents: String
    let extracurricular_activities: String?
    let schoolSports: String?
    let attendanceRate: String?
    let pct_stu_enough_variety:String?
    let pct_stu_safe: String?
    let school_accessibility_description: String?
    let directions1: String?
    let requirement2_1: String?
    let requirement3_1: String?
    let requirement4_1: String?
    let requirement5_1: String?
    let offer_rate1: String?
    let program1: String?
    let code1: String?
    let interest1: String?
    let method1: String?
    let seats9ge1: String?
    let grade9gefilledflag1: String?
    let grade9geapplicants1: String?
    let seats9swd1: String?
    let grade9swdfilledflag1: String?
    let grade9swdapplicants1: String?
    let seats101: String?
    let admissionspriority11: String?
    let admissionspriority21: String?
    let admissionspriority31: String?
    let grade9geapplicantsperseat1: String?
    let grade9swdapplicantsperseat1: String?
    let primaryAddress: String
    let city: String
    let zip: String
    let stateCode: String
    let latitude: String?
    let longitude: String?
    let community_board: String?
    let council_district: String?
    let census_tract: String?
    let bin: String?
    let bbl: String?
    let nta: String?
    let borough: String?
    let language: String?
    
    var address: String {
        return "\(primaryAddress), \(city), \(stateCode), \(zip)"
    }
    
    var contactUS: String {
        guard let location = location, let phoneNumber = phoneNumber, let faxNumber = faxNumber, let email = schoolEmail else {
            return "NA"
        }
        return "\(location) \nPhone: \(phoneNumber) \nFax: \(faxNumber) \nEmail: \(email)"
    }

    var transport: String {
        guard let bus = bus, let subway = subway else {
            return "NA"
        }
        return "Bus: \(bus)\nSubway: \(subway)"
    }

    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case schoolName = "school_name"
        case boro
        case description = "overview_paragraph"
        case school10thSeats = "school_10th_seats"
        case academicopportunities1
        case ellPrograms = "ell_programs"
        case neighborhood
        case building_code
        case location
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case schoolEmail = "school_email"
        case website
        case subway
        case bus
        case grades2018
        case finalgrades
        case totalStudents = "total_students"
        case extracurricular_activities
        case schoolSports = "school_sports"
        case attendanceRate = "attendance_rate"
        case pct_stu_enough_variety
        case pct_stu_safe
        case school_accessibility_description = "school_accessibility_description"
        case directions1
        case requirement2_1
        case requirement3_1
        case requirement4_1
        case requirement5_1
        case offer_rate1
        case program1
        case code1
        case interest1
        case method1
        case seats9ge1
        case grade9gefilledflag1
        case grade9geapplicants1
        case seats9swd1
        case grade9swdfilledflag1
        case grade9swdapplicants1
        case seats101
        case admissionspriority11
        case admissionspriority21
        case admissionspriority31
        case grade9geapplicantsperseat1
        case grade9swdapplicantsperseat1
        case community_board
        case council_district
        case census_tract
        case bin
        case bbl
        case nta
        case borough
        case primaryAddress = "primary_address_line_1"
        case city
        case zip
        case stateCode = "state_code"
        case latitude
        case longitude
        case language = "language_classes"
    }
}
