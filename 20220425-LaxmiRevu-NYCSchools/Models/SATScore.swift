//
//  SATScore.swift
//  20220425-LaxmiRevu-NYCSchools
//
//  Created by Laxmi Revu on 4/25/22.
//

import Foundation

struct SATScore: Decodable {
    let id: String
    let schoolName: String
    let testTakers: String
    let readingAvgScore: String?
    let mathAvgScore: String?
    let writingAvgScore: String?
    
    var mathScore: Int {
        guard let math = mathAvgScore, let score = Int(math) else {
            return 0
        }
        return score
    }
    var readingScore: Int {
        guard let reading = readingAvgScore, let score = Int(reading) else {
            return 0
        }
        return score
    }
    var writingScore: Int {
        guard let writing = writingAvgScore, let score = Int(writing) else {
            return 0
        }
        return score
    }

    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case schoolName = "school_name"
        case testTakers = "num_of_sat_test_takers"
        case readingAvgScore = "sat_critical_reading_avg_score"
        case mathAvgScore = "sat_math_avg_score"
        case writingAvgScore = "sat_writing_avg_score"
    }
}
