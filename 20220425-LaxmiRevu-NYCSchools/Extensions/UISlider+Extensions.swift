//
//  UISlider+Extensions.swift
//  20220425-LaxmiRevu-NYCSchools
//
//  Created by Laxmi Revu on 4/26/22.
//

import UIKit
private let belowAvarageScore: Float = 500
private let aboveAverageScore: Float = 1000
private let sliderHeight: CGFloat = 10

class CustomSlider: UISlider {
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        let point = CGPoint(x: bounds.minX, y: bounds.midY)
        //this height is the thickness
        return CGRect(origin: point, size: CGSize(width: bounds.width, height: sliderHeight))
    }
    
    /*
      - Below avaerage SAT score is 500 - slider shows red color
      - Avaerage SAT score is above 500 and below 1000 - slider shows yellow color
      - Above avaerage SAT score is 1000 - slider shows green color
     */
    func updateSliderColor() {
        if self.value <= belowAvarageScore {
            self.minimumTrackTintColor = .red
        } else if self.value > belowAvarageScore && self.value <= aboveAverageScore {
            self.minimumTrackTintColor = .yellow
        } else {
            self.minimumTrackTintColor = UIColor(hexString: "#0B6623")
        }
    }

}
